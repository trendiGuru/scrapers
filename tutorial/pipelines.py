# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import cv2
import numpy as np
import string
from Tkinter import Tk
from tkFileDialog import askopenfilename
from scrapy.exceptions import DropItem
import background_removal
import Utils



# class TutorialPipeline(object):
#     def process_item(self, item, spider):
#         file = open('~/Desktop/trendiFile.txt', 'w+')
#         file.write(item)
#         return item

#Checks and filters duplicates but mostly, checks if image is relevant for our purposes,
#meaning, that the image, most likely has a person (at least one) in it, and its face
#doesn't take over the whole image - the face body ratio is relatively low.
#we use Lior's "get_cv2_img_array" from Utils module to convert a url into an image, then
#we use Nadav's "image_is_relevant" from background_removal module to filter images with a
#lousy face to body ratio.
class DuplicatesPipeline(object):

    def __init__(self):
        self.ids_seen = set()


    def process_item(self, item, spider):
        #Duplicate:
        if item['url'] in self.ids_seen:
            print "DUUUUUUUUUUUUUUUUUUUUUUplicated item"
            raise DropItem("Duplicate item found: {0}".format(item))
        else:

            #Valid url:
            if "url" in item and item["url"]:

                self.ids_seen.add(item['url'])
                img = Utils.get_cv2_img_array(item['url'])
                #Indeed there's a convertion from url to image:
                if img is not None:
                    relevant = background_removal.image_is_relevant(img)
                    if relevant == True:
                        return item
                    else:
                        print "not relevant!!!!!!!!!!!!!!!!!!!!!"
                else:
                    print "not img!!!!!!!!!!!!!!!!!!!!!!!!!!"

            raise DropItem("Wasn't relevant")

