import scrapy
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
import background_removal
import Utils

from tutorial.items import DmozItem

class DmozSpider(CrawlSpider):
    name = "dmoz"
    allowed_domains = ["gala.de"]
    start_urls = ["http://www.gala.de/"]

    rules = (Rule(SgmlLinkExtractor(deny =["http://www.gala.de/beauty-fashion/beauty/",
                                           "http://shop.gala.de/abonnements/","http://www.gala.de/videos/",
                                           "http://shopfinder.gala.de/accessoires",
                                           "http://shopfinder.gala.de/bekleidung",
                                           "http://www.gala.de/lifestyle/spiele/"])
                  , callback='parse_items', follow=True),)

    def parse_items(self, response):

        sel = Selector(response)

        images_from_galleries = sel.xpath('//div[@class = "image-gallery-wrap"]//figure[@class = "img gallery-img"]'
                                          '//img[contains (@src, ".jpg")]')
        images_from_articles = sel.xpath('//section[@class = "content"]//a[contains (@href,".jpg")]')

        items = []

        for img in images_from_galleries:
            item = DmozItem()
            extracted_sources = img.xpath('@src').extract()
            extracted_titles = img.xpath('@title').extract()
             # isn't an empty list:
            if extracted_sources:
                #we assume that there's only one image source - we want the url:
                item['url'] = extracted_sources[0]
                #if there's a title we take it, else, "no title"
                item['title'] = extracted_titles[0] if extracted_titles else "no_title"
                item['taken_from'] = "gallery"
                items.append(item)

        #Same comments from above apply here too:
        for img in images_from_articles:
            item = DmozItem()
            extracted_hrefs = img.xpath('@href').extract()
            extracted_titles = img.xpath('@title').extract()
            if extracted_hrefs:
                item['url'] = extracted_hrefs[0]
                item['title'] = extracted_titles[0] if extracted_titles else "no_title"
                item['taken_from'] = "article"
                items.append(item)

        return items

