# -*- coding: utf-8 -*-

# Scrapy settings for tutorial project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

#Feel free to change settings:
BOT_NAME = 'tutorial'
SPIDER_MODULES = ['tutorial.spiders']
NEWSPIDER_MODULE = 'tutorial.spiders'
#Maximum number of concurrent items (per response) to process in parallel in the pipeline:
CONCURRENT_ITEMS = 100 #100 is the default number.
#The maximum number of concurrent (ie. simultaneous) requests that will be performed by the Scrapy downloader.
CONCURRENT_REQUESTS = 100
ITEM_PIPELINES = {
    'tutorial.pipelines.DuplicatesPipeline': 300
}
DOWNLOAD_TIMEOUT = 180


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'tutorial (+http://www.yourdomain.com)'
